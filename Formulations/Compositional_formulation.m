% Compositional Formulation base class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DARSim 2 Reservoir Simulator
%Author: Matteo Cusini
%TU Delft
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Compositional_formulation < formulation
    properties
        NofComponents
        
        Utot
    end
    methods
        function obj = Compositional_formulation(n_components)
            obj@formulation();
            obj.NofComponents = n_components;
%             obj.Tph = cell(n_components, 2);
%             obj.Gph = cell(n_components, 2);

            obj.Tph = cell(4, 1); % c1ph1, c1ph2, c2ph1, c2ph2
            obj.Gph = cell(4, 1);
        end
        function  TransmissibilityMatrix(obj, Grid, Rho, x, Index, i, f)
            %%%Transmissibility matrix construction
            Tx = zeros(Grid.Nx+1, Grid.Ny, Grid.Nz);
            Ty = zeros(Grid.Nx, Grid.Ny+1, Grid.Nz);
            Tz = zeros(Grid.Nx, Grid.Ny, Grid.Nz+1);
            
            %% PHASE 1 
            %Apply upwind operator
            Mupx = obj.UpWind{1,1+f}.x*(obj.Mob(Index.Start:Index.End,1) .* Rho(:,1) .* x(:,1)); 
            Mupy = obj.UpWind{1,1+f}.y*(obj.Mob(Index.Start:Index.End,1) .* Rho(:,1) .* x(:,1));
            Mupz = obj.UpWind{1,1+f}.z*(obj.Mob(Index.Start:Index.End,1) .* Rho(:,1) .* x(:,1));
            Mupx = reshape(Mupx, Grid.Nx, Grid.Ny, Grid.Nz);
            Mupy = reshape(Mupy, Grid.Nx, Grid.Ny, Grid.Nz);
            Mupz = reshape(Mupz, Grid.Nx, Grid.Ny, Grid.Nz);
            
            Tx(2:Grid.Nx,:,:)= Grid.Tx(2:Grid.Nx,:,:).*Mupx(1:Grid.Nx-1,:,:);
            Ty(:,2:Grid.Ny,:)= Grid.Ty(:,2:Grid.Ny,:).*Mupy(:,1:Grid.Ny-1,:);
            Tz(:,:,2:Grid.Nz)= Grid.Tz(:,:,2:Grid.Nz).*Mupz(:,:,1:Grid.Nz-1);
            
            %Construct matrix
            x1 = reshape(Tx(1:Grid.Nx,:,:), Grid.N, 1);
            x2 = reshape(Tx(2:Grid.Nx+1,:,:), Grid.N, 1);
            y1 = reshape(Ty(:,1:Grid.Ny,:), Grid.N, 1);
            y2 = reshape(Ty(:,2:Grid.Ny+1,:), Grid.N, 1);
            z1 = reshape(Tz(:,:,1:Grid.Nz), Grid.N, 1);
            z2 = reshape(Tz(:,:,2:Grid.Nz+1), Grid.N, 1);
            
            DiagVecs = [-z2,-y2,-x2,z2+y2+x2+y1+x1+z1,-x1,-y1,-z1];
            DiagIndx = [-Grid.Nx*Grid.Ny, -Grid.Nx, -1, 0, 1, Grid.Nx, Grid.Nx*Grid.Ny];
            % obj.Tph{i,1} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            obj.Tph{2*i-1,1+f} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            
            % Gravity Matrix
            Tx(2:Grid.Nx,:,:)= Tx(2:Grid.Nx,:,:) .* obj.GravityModel.RhoInt{1,1+f}.x(2:Grid.Nx,:,:);
            Ty(:,2:Grid.Ny,:)= Ty(:,2:Grid.Ny,:) .* obj.GravityModel.RhoInt{1,1+f}.y(:,2:Grid.Ny,:);
            Tz(:,:,2:Grid.Nz)= Tz(:,:,2:Grid.Nz) .* obj.GravityModel.RhoInt{1,1+f}.z(:,:,2:Grid.Nz);
            
            %Construct matrix
            x1 = reshape(Tx(1:Grid.Nx,:,:), Grid.N, 1);
            x2 = reshape(Tx(2:Grid.Nx+1,:,:), Grid.N, 1);
            y1 = reshape(Ty(:,1:Grid.Ny,:), Grid.N, 1);
            y2 = reshape(Ty(:,2:Grid.Ny+1,:), Grid.N, 1);
            z1 = reshape(Tz(:,:,1:Grid.Nz), Grid.N, 1);
            z2 = reshape(Tz(:,:,2:Grid.Nz+1), Grid.N, 1);
            
            DiagVecs = [-z2,-y2,-x2,z2+y2+x2+y1+x1+z1,-x1,-y1,-z1];
            DiagIndx = [-Grid.Nx*Grid.Ny, -Grid.Nx, -1, 0, 1, Grid.Nx, Grid.Nx*Grid.Ny];
            % obj.Gph{i,1} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            obj.Gph{2*i-1,1+f} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            
            %% PHASE 2 
            %Apply upwind operator
            Mupx = obj.UpWind{2,1+f}.x*(obj.Mob(Index.Start:Index.End,2) .* Rho(:,2) .* x(:,2));
            Mupy = obj.UpWind{2,1+f}.y*(obj.Mob(Index.Start:Index.End,2) .* Rho(:,2) .* x(:,2));
            Mupz = obj.UpWind{2,1+f}.z*(obj.Mob(Index.Start:Index.End,2) .* Rho(:,2) .* x(:,2));
            Mupx = reshape(Mupx, Grid.Nx, Grid.Ny, Grid.Nz);
            Mupy = reshape(Mupy, Grid.Nx, Grid.Ny, Grid.Nz);
            Mupz = reshape(Mupz, Grid.Nx, Grid.Ny, Grid.Nz);
            
            Tx(2:Grid.Nx,:,:) = Grid.Tx(2:Grid.Nx,:,:).*Mupx(1:Grid.Nx-1,:,:);
            Ty(:,2:Grid.Ny,:) = Grid.Ty(:,2:Grid.Ny,:).*Mupy(:,1:Grid.Ny-1,:);
            Tz(:,:,2:Grid.Nz) = Grid.Tz(:,:,2:Grid.Nz).*Mupz(:,:,1:Grid.Nz-1);
            
            %Construct matrix
            x1 = reshape(Tx(1:Grid.Nx,:,:), Grid.N, 1);
            x2 = reshape(Tx(2:Grid.Nx+1,:,:), Grid.N, 1);
            y1 = reshape(Ty(:,1:Grid.Ny,:), Grid.N, 1);
            y2 = reshape(Ty(:,2:Grid.Ny+1,:), Grid.N, 1);
            z1 = reshape(Tz(:,:,1:Grid.Nz), Grid.N, 1);
            z2 = reshape(Tz(:,:,2:Grid.Nz+1), Grid.N, 1);
            
            DiagVecs = [-z2,-y2,-x2,z2+y2+x2+y1+x1+z1,-x1,-y1,-z1];
            DiagIndx = [-Grid.Nx*Grid.Ny, -Grid.Nx, -1, 0, 1, Grid.Nx, Grid.Nx*Grid.Ny];
            % obj.Tph{i,2} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            obj.Tph{2*i,1+f} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            
            % Gravity Matrix
            Tx(2:Grid.Nx,:,:) = Tx(2:Grid.Nx,:,:) .* obj.GravityModel.RhoInt{2,1+f}.x(2:Grid.Nx,:,:);
            Ty(:,2:Grid.Ny,:) = Ty(:,2:Grid.Ny,:) .* obj.GravityModel.RhoInt{2,1+f}.y(:,2:Grid.Ny,:);
            Tz(:,:,2:Grid.Nz) = Tz(:,:,2:Grid.Nz) .* obj.GravityModel.RhoInt{2,1+f}.z(:,:,2:Grid.Nz);
            
            %Construct matrix
            x1 = reshape(Tx(1:Grid.Nx,:,:), Grid.N, 1);
            x2 = reshape(Tx(2:Grid.Nx+1,:,:), Grid.N, 1);
            y1 = reshape(Ty(:,1:Grid.Ny,:), Grid.N, 1);
            y2 = reshape(Ty(:,2:Grid.Ny+1,:), Grid.N, 1);
            z1 = reshape(Tz(:,:,1:Grid.Nz), Grid.N, 1);
            z2 = reshape(Tz(:,:,2:Grid.Nz+1), Grid.N, 1);
            
            DiagVecs = [-z2,-y2,-x2,z2+y2+x2+y1+x1+z1,-x1,-y1,-z1];
            DiagIndx = [-Grid.Nx*Grid.Ny, -Grid.Nx, -1, 0, 1, Grid.Nx, Grid.Nx*Grid.Ny];
            % obj.Gph{i,2} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            obj.Gph{2*i,1+f} = spdiags(DiagVecs, DiagIndx, Grid.N, Grid.N);
            
        end
        function q = ComputeSourceTerms(obj, N, Wells)
            q = zeros(N, obj.NofComponents);
            %Injectors
            for i=1:Wells.NofInj
                c = Wells.Inj(i).Cells;
                q(c, :) = Wells.Inj(i).QComponents(:,:);
            end
            %Producers
            for i=1:Wells.NofProd
                c = Wells.Prod(i).Cells;
                q(c, :) = Wells.Prod(i).QComponents(:,:);
            end
        end
        function AverageMassOnCoarseBlocks(obj, Status, FluidModel, R, P)
            % Perform Average for ADM
            for i=1:obj.NofComponents
                z = Status.Properties(['z_',num2str(i)]);
                z_rest = R * z.Value;
                z.Value = P * z_rest;
            end
            
            % Update other unknwons as well 
            obj.UpdatePhaseCompositions(Status, FluidModel);
        end
        function UpdatePhaseCompositions(obj, Medium, DiscretizationModel, FluidModel, label)
            %% 2. Perform composition update ((x, ni) = f(T, p, z))
            % obj.SinglePhase = FluidModel.Flash(Reservoir.State);
            
            SinglePhase = FluidModel.Flash(Medium.State);
            status_SinglePhase = Medium.State.Properties('SinglePhase');
            status_SinglePhase.Value = SinglePhase;
            
            %% 3. Compute Densities (rho = rho(p, x, T))
            % FluidModel.ComputePhaseDensities(Reservoir.State, obj.SinglePhase);
            
            FluidModel.ComputePhaseDensities(Medium.State, Medium.State.Properties('SinglePhase').Value);
            
            %% 4. Compute Saturations (S = S(z, x))
            % FluidModel.ComputePhaseSaturation(Reservoir.State, obj.SinglePhase);
            
            FluidModel.ComputePhaseSaturation(Medium.State, Medium.State.Properties('SinglePhase').Value);         
            
            %% 5. Compute Total Density (rhoT = rhoT(S, rho))
            FluidModel.ComputeTotalDensity(Medium.State);
            
            %% 6. Compute Pc (Pc = Pc(S))
            FluidModel.ComputePc(Medium.State, DiscretizationModel, label);
            
        end
        function ComputeTotalFluxes(obj, ProductionSystem, DiscretizationModel)
            Nx = DiscretizationModel.ReservoirGrid.Nx;
            Ny = DiscretizationModel.ReservoirGrid.Ny;
            Nz = DiscretizationModel.ReservoirGrid.Nz;
            N  = DiscretizationModel.ReservoirGrid.N;
             
            obj.Utot.x = zeros(Nx+1, Ny, Nz);
            obj.Utot.y = zeros(Nx, Ny+1, Nz);
            obj.Utot.z = zeros(Nx, Ny, Nz+1);
            
            for ph = 1:obj.NofPhases
                obj.Utot.x(2:Nx+1,:,:) = obj.Utot.x(2:Nx+1,:,:) + obj.U{ph, 1}.x(2:Nx+1,:,:) .* reshape(obj.UpWind{ph,1}.x *  (obj.Mob(1:N, ph)), Nx, Ny, Nz);  %- Ucap.x(2:Nx,:);
                obj.Utot.y(:,2:Ny+1,:) = obj.Utot.y(:,2:Ny+1,:) + obj.U{ph, 1}.y(:,2:Ny+1,:) .* reshape(obj.UpWind{ph,1}.y *  (obj.Mob(1:N, ph)), Nx, Ny, Nz);  %- Ucap.y(:,2:Ny);
                obj.Utot.z(:,:,2:Nz+1) = obj.Utot.z(:,:,2:Nz+1) + obj.U{ph, 1}.z(:,:,2:Nz+1) .* reshape(obj.UpWind{ph,1}.z *  (obj.Mob(1:N, ph)), Nx, Ny, Nz);  %- Ucap.y(:,2:Ny);
            end
            

%             obj.Utot.x(2:Nx+1,:,:) = obj.Utot.x(2:Nx+1,:,:) + obj.U{2, 1}.x(2:Nx+1,:,:) .* reshape(obj.UpWind{2,1}.x *  (obj.Mob(1:N, 2)), Nx, Ny, Nz);  %- Ucap.x(2:Nx,:);
%             % obj.Utot.y(:,2:Ny+1,:) = obj.Utot.y(:,2:Ny+1,:) + obj.U{ph, 1}.y(:,2:Ny+1,:) .* reshape(obj.UpWind{ph,1}.y *  (obj.Mob(1:N, ph)), Nx, Ny, Nz);  %- Ucap.y(:,2:Ny);
%             obj.Utot.z(:,:,2:Nz+1) = obj.Utot.z(:,:,2:Nz+1) + obj.U{2, 1}.z(:,:,2:Nz+1) .* reshape(obj.UpWind{2,1}.z *  (obj.Mob(1:N, 2)), Nx, Ny, Nz);  %- Ucap.y(:,2:Ny);

%             ux_temp = obj.Utot.x(2:Nx+1,:,:);
%             status_ux = ProductionSystem.Reservoir.State.Properties('ux');
%             status_ux.Value = ux_temp(:);
%             
%             uz_temp = obj.Utot.z(:,:,2:Nz+1);
%             status_uz = ProductionSystem.Reservoir.State.Properties('uz');
%             status_uz.Value = uz_temp(:); 
        end
            
        function conservative = CheckMassConservation(obj, DiscretizationModel)
            %Checks mass balance in all cells     
            Nx = DiscretizationModel.ReservoirGrid.Nx;
            Ny = DiscretizationModel.ReservoirGrid.Ny;
            Nz = DiscretizationModel.ReservoirGrid.Nz;
            N  = DiscretizationModel.ReservoirGrid.N;
            
            conservative = 'Pass';
            maxUx = max(max(max(obj.Utot.x)));
            maxUy = max(max(max(obj.Utot.y)));
            maxUz = max(max(max(obj.Utot.z)));
            maxU = max([maxUx, maxUy, maxUz]);
            ux = reshape(obj.Utot.x(1:Nx,:,:) - obj.Utot.x(2:Nx+1,:,:), N, 1);
            uy = reshape(obj.Utot.y(:,1:Ny,:) - obj.Utot.y(:,2:Ny+1,:), N, 1);
            uz = reshape(obj.Utot.z(:,:,1:Nz) - obj.Utot.z(:,:,2:Nz+1), N, 1);
            
            Balance = ux + uy + uz; % wells are not included
            if norm(Balance, inf) > 1e-5
                conservative = 'Failed: Mass is NOT conserved!';
            end
            disp(['Mass conservation check:' num2str(conservative)]);
        end
        function CFL = ComputeCFLNumber(obj, ProductionSystem, DiscretizationModel, dt)
%             N = DiscretizationModel.ReservoirGrid.N;      
%             pv = ProductionSystem.Reservoir.Por*DiscretizationModel.ReservoirGrid.Volume;
%             P = zeros(N, obj.NofPhases);
%             z = zeros(N, obj.NofComponents);
%             rho = zeros(N, obj.NofPhases);
%             x = zeros(N, obj.NofComponents*obj.NofPhases);
%             % Copy values in local variables
%             for i=1:obj.NofPhases
%                 P(:, i) = ProductionSystem.Reservoir.State.Properties(['P_', num2str(i)]).Value;
%                 rho(:, i) = ProductionSystem.Reservoir.State.Properties(['rho_', num2str(i)]).Value;
%             end
%             for i=1:obj.NofComponents
%                 z(:, i) = ProductionSystem.Reservoir.State.Properties(['z_', num2str(i)]).Value;
%                 for j=1:obj.NofComponents
%                     x(:,(i-1)*obj.NofPhases + j) = ProductionSystem.Reservoir.State.Properties(['x_', num2str(i),'ph',num2str(j)]).Value;
%                 end
%             end
%             rhoT =  ProductionSystem.Reservoir.State.Properties('rhoT').Value;
%             
%              % Depths
%             depth = DiscretizationModel.ReservoirGrid.Depth;
%             
%             % Source terms
%             q = obj.ComputeSourceTerms(N, ProductionSystem.Wells);
%             
%             ThroughPut = zeros(N, obj.NofComponents);
%             Mass = zeros(N, obj.NofComponents);
%             for i=1:obj.NofComponents
%                 obj.TransmissibilityMatrix(DiscretizationModel.ReservoirGrid, rho, obj.GravityModel.RhoInt, x(:,(i-1)*2+1:(i-1)*2+2), i);
%                 % d1 = tril(obj.Tph{i, 1}, -1); 
%                 % d2 = tril(obj.Tph{i, 2}, -1);
%                 % u1 = triu(obj.Tph{i, 1},  1);
%                 % u2 = triu(obj.Tph{i, 2},  1);
%                 d1 = tril(obj.Tph{2*i-1, 1}, -1); 
%                 d2 = tril(obj.Tph{2*i, 1}, -1);
%                 u1 = triu(obj.Tph{2*i-1, 1},  1);
%                 u2 = triu(obj.Tph{2*i, 1},  1);
%                 
%                 % Diag1 = diag(obj.Tph{i, 1});
%                 % Diag2 = diag(obj.Tph{i, 2});
%                 Diag1 = diag(obj.Tph{2*i-1, 1});
%                 Diag2 = diag(obj.Tph{2*i, 1});
%                 
%                 % Assemble
%                 D1 = diag(Diag1 + sum(u1, 2)) + d1; 
%                 D2 = diag(Diag2 + sum(u2, 2)) + d2;
%                 U1 = diag(Diag1 + sum(d1, 2)) + u1;
%                 U2 = diag(Diag2 + sum(d2, 2)) + u2;
%                 ThroughPut(:,i) = ...
%                            - min(D1 * P(:,1), 0)...    % Convective term                
%                            - min(D2 * P(:,2), 0)...
%                            - min(U1 * P(:,1), 0)...    % Convective term                
%                            - min(U2 * P(:,2), 0)...
%                            + max(q(:,i), 0);                       % Wells
%                 Mass(:,i) = rhoT .* z(:,i);
%             end
%             Mass = max(Mass, 1e-10);
%             ThroughPut(ThroughPut < 1e-10) = 0;
%             Ratio = ThroughPut ./ Mass;
%             CFL = dt * max(max(Ratio));
        CFL = 1;
        end
    end
end